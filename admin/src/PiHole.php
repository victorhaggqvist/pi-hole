<?php
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * User: Victor Häggqvist
 * Date: 2/17/16
 * Time: 12:23 AM
 */
class PiHole {

    public static function getStats() {
        $process = new Process("/usr/local/bin/chronometer.sh -j");
        $process->mustRun();
        return $process->getOutput();
    }

    public static function getVersion() {
        $process = new Process("git describe --tags --abbrev=0", "/etc/.pihole");
        try {
            $process->mustRun();
            return $process->getOutput();
        } catch (ProcessFailedException $e) {
            return "Failed to fetch version";
        }
    }

    public static function getWebVersion() {
        $process = new Process("git describe --tags --abbrev=0", "/var/www/html/admin/");
        try {
            $process->mustRun();
            return $process->getOutput();
        } catch (ProcessFailedException $e) {
            return "Failed to fetch version";
        }
    }

}
