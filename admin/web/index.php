<?php

use Symfony\Component\HttpFoundation\Response;

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views',
));

$app->get('/api', function() use($app) {
    return new Response(PiHole::getStats(), 200, array('Content-Type' => 'application/json'));
});

$app->get('/', function() use($app) {
    $stats = json_decode(PiHole::getStats(), true);

    return $app['twig']->render('index.html.twig', array(
        'stats' => $stats,
        'version' => PiHole::getVersion(),
        'web' => PiHole::getWebVersion()
    ));
});

$app->run();
