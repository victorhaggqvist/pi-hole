apt-get -q update && apt-get upgrade -y
apt-get install -y \
        dnsmasq \
        php5-common php5-fpm php5 \
        bc curl unzip wget nginx git

tmpLog=/tmp/pihole-install.log
instalLogLoc=/etc/pihole/install.log

webInterfaceGitUrl="https://github.com/pi-hole/AdminLTE.git"
webInterfaceDir="/var/www/html/admin"
piholeGitUrl="https://github.com/pi-hole/pi-hole.git"
piholeFilesDir="/etc/.pihole"

installScripts() {
	# Install the scripts from /etc/.pihole to their various locations
	$SUDO echo ":::"
	$SUDO echo -n "::: Installing scripts..."
	$SUDO cp /etc/.pihole/gravity.sh /usr/local/bin/gravity.sh
	$SUDO cp /etc/.pihole/advanced/Scripts/chronometer.sh /usr/local/bin/chronometer.sh
	$SUDO cp /etc/.pihole/advanced/Scripts/whitelist.sh /usr/local/bin/whitelist.sh
	$SUDO cp /etc/.pihole/advanced/Scripts/blacklist.sh /usr/local/bin/blacklist.sh
	$SUDO cp /etc/.pihole/advanced/Scripts/piholeLogFlush.sh /usr/local/bin/piholeLogFlush.sh
	$SUDO cp /etc/.pihole/advanced/Scripts/updateDashboard.sh /usr/local/bin/updateDashboard.sh
	$SUDO chmod 755 /usr/local/bin/{gravity,chronometer,whitelist,blacklist,piholeLogFlush,updateDashboard}.sh
	$SUDO echo " done."
}

installConfigs() {
	# Install the configs from /etc/.pihole to their various locations
	$SUDO echo ":::"
	$SUDO echo "::: Installing configs..."
  ln -sf /vagrant/nginx.conf /etc/nginx/sites-enabled/default
}



getGitFiles() {
	# Setup git repos for base files and web admin
	echo ":::"
	echo "::: Checking for existing base files..."
	if is_repo $piholeFilesDir; then
		make_repo $piholeFilesDir $piholeGitUrl
	else
		update_repo $piholeFilesDir
	fi

	echo ":::"
	echo "::: Checking for existing web interface..."
	if is_repo $webInterfaceDir; then
		make_repo $webInterfaceDir $webInterfaceGitUrl
	else
		update_repo $webInterfaceDir
	fi
}

is_repo() {
	# If the directory does not have a .git folder it is not a repo
	echo -n ":::    Checking $1 is a repo..."
    if [ -d "$1/.git" ]; then
    		echo " OK!"
        return 1
    fi
    echo " not found!!"
    return 0
}

make_repo() {
    # Remove the non-repod interface and clone the interface
    echo -n ":::    Cloning $2 into $1..."
    $SUDO rm -rf $1
    $SUDO git clone -q "$2" "$1" > /dev/null
    echo " done!"
}

update_repo() {
    # Pull the latest commits
    echo -n ":::     Updating repo in $1..."
    cd "$1"
    $SUDO git pull -q > /dev/null
    echo " done!"
}


CreateLogFile() {
	# Create logfiles if necessary
	echo ":::"
	$SUDO  echo -n "::: Creating log file and changing owner to dnsmasq..."
	if [ ! -f /var/log/pihole.log ]; then
		$SUDO touch /var/log/pihole.log
		$SUDO chmod 644 /var/log/pihole.log
		$SUDO chown dnsmasq:root /var/log/pihole.log
		$SUDO echo " done!"
	else
		$SUDO  echo " already exists!"
	fi
}

installPiholeWeb() {
	# Install the web interface
	$SUDO echo ":::"
	$SUDO echo -n "::: Installing pihole custom index page..."
	if [ -d "/var/www/html/pihole" ]; then
		$SUDO echo " Existing page detected, not overwriting"
	else
		$SUDO mkdir /var/www/html/pihole
		$SUDO mv /var/www/html/index.lighttpd.html /var/www/html/index.lighttpd.orig
		$SUDO cp /etc/.pihole/advanced/index.html /var/www/html/pihole/index.html
		$SUDO echo " done!"
	fi
}

mkdir -p /etc/pihole/
chown www-data:www-data /var/www/html
chmod 775 /var/www/html
usermod -a -G www-data vagrant

getGitFiles
installScripts
installConfigs
#installWebAdmin
CreateLogFile
#installPiholeWeb

openssl req -newkey rsa:8192 -sha256 -subj "/AU=Foo/C=SE/ST=Some were/L=Some were/O=Bogus Company/CN=hole.pi.example.com" -nodes -keyout pihole.key -out pihole.csr
openssl x509 -signkey pihole.key -in pihole.csr -req -days 365 -out pihole.crt
sudo mkdir -p /etc/nginx/ssl
sudo cp pihole.crt /etc/nginx/ssl
sudo cp pihole.key /etc/nginx/ssl
sudo openssl dhparam -out /etc/nginx/ssl/dhparam.pem 2024
