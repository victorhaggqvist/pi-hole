wget https://github.com/pi-hole/pi-hole/raw/master/automated%20install/basic-install.sh
chmox +x basic-install.sh
./basic-install.sh

apt-get -y install nginx php5-fpm apache2-utils
systemctl stop lighttpd.service
systemctl disable lighttpd.service
systemctl enable php5-fpm.service
systemctl enable nginx.service

htpasswd -c /etc/nginx/.htpasswd $0 # param for web username

openssl req -newkey rsa:8192 -sha256 -subj "/AU=Foo/C=SE/ST=Some were/L=Some were/O=Bogus Company/CN=hole.pi.example.com" -nodes -keyout pihole.key -out pihole.csr
openssl x509 -signkey pihole.key -in pihole.csr -req -days 365 -out pihole.crt
mkdir -p /etc/nginx/ssl
cp pihole.crt /etc/nginx/ssl
cp pihole.key /etc/nginx/ssl
openssl dhparam -out /etc/nginx/ssl/dhparam.pem 2024
systemctl restart nginx.service
systemctl restart php5-fpm.service
