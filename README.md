[pi-hole](https://pi-hole.net) with modifications

- Silex for admin web
- nginx instead of lighttp
- TLS and basic auth for admin web
- admin web on 8080

Changes are done on top of pi-hole.


Clone repo and run

    setup.sh username

where username is the webuser.
